package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/smtp"
	"os"
	"time"

	"github.com/Sirupsen/logrus"
	"github.com/alecthomas/kingpin"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/olebedev/config"
)

var (
	app        = kingpin.New("SecureMe", "SecureMe Honeypot").DefaultEnvars()
	listenPort = app.Flag("port", "This daemon runs on").Short('l').Default("1323").OverrideDefaultFromEnvar("PORT").String()
	//debug      = app.Flag("debug", "Debug").Short('d').Envar("DEBUG").Bool()
	configfile = app.Flag("config", "Configuration file path").Short('c').Default("config.yaml").OverrideDefaultFromEnvar("CONFIGFILE").String()
	logr       = logrus.New()
)

func runApp(cfg *config.Config) {
	e := echo.New()
	e.Use(middleware.SecureWithConfig(middleware.SecureConfig{
		XSSProtection:      "",
		ContentTypeNosniff: "",
		XFrameOptions:      "",
		HSTSMaxAge:         3600,
	}))

	e.File("/", "static/index.html")
	e.File("/robots.txt", "static/robots.txt")
	e.Static("/assets", "static/assets")

	e.POST("/", func(c echo.Context) error {
		//email := c.FormValue("email")
		//password := c.FormValue("password")
		mailfrom := cfg.UString("mailfrom")
		recipient := cfg.UString("recipient")
		smtpserver := cfg.UString("smtpserver")

		cm, err := smtp.Dial(fmt.Sprintf("%s:25", smtpserver))
		if err != nil {
			log.Fatal(err)
		}
		defer cm.Close()
		// Set the sender and recipient.
		cm.Mail(mailfrom)
		cm.Rcpt(recipient)
		// Send the email body.
		wc, err := cm.Data()
		if err != nil {
			log.Fatal(err)
		}
		defer wc.Close()

		ipuser := c.RealIP()
		t := time.Now()
		body := fmt.Sprintf("From: %s\r\nDate: "+t.Format("Mon, _2 Jan 2006 15:04:05 -0700")+"\r\nTo: %s\r\nSubject: Acces Login 192.168.24.10\r\nFrom user %s", mailfrom, recipient, ipuser)

		buf := bytes.NewBufferString(body)
		if _, err = buf.WriteTo(wc); err != nil {
			log.Fatal(err)
		}
		return c.NoContent(http.StatusNoContent)
	})

	// We block, waiting for a happy ending...
	e.Logger.Fatal(e.Start(fmt.Sprintf(":%s", *listenPort)))
}

func main() {
	logr.Infof("Starting Application")
	app.Version("0.0.1")
	kingpin.MustParse(app.Parse(os.Args[1:]))

	cfgsource, err := ioutil.ReadFile(*configfile)
	if err != nil {
		logr.Fatal(err)
	}

	cfg, err := config.ParseYaml(string(cfgsource))
	if err != nil {
		logr.Fatal(err)
	}

	if *listenPort != "" {
		cfg.Set("listenPort", *listenPort)
	}

	/* *************** */
	runApp(cfg)
}
